
import React, { useEffect, useState } from 'react';
import { Card, Title, Button } from 'react-native-paper';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import FormInput from '../styles/FormInput';
import FormButton from '../styles/FormButton';
import { windowHeight, windowWidth } from '../util/dimension';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { SafeAreaView, StyleSheet, Text, TextInput, View, Image, TouchableOpacity, ScrollView } from "react-native";
import { Searchbar } from 'react-native-paper';

const LoginScreen = () => {
  const [userName, setUserName] = useState('');
  const [log, setLog] = useState(false);
  const [password, setPassword] = useState();
  const [email, setEmail] = useState();
  const [find, setFind] = useState()
  const [visible, setVisible] = useState(true);
 

  const getResponseInfo = (error, result) => {
    console.log("ddd")
    if (error) {
      //Alert for the Error
      alert('Error fetching data: ' + error.toString());
    } else {
      //response alert
      console.log("ddd")
      console.log(JSON.stringify(result));
      setUserName(result.name);
    
    }
  };

  const onLogout = () => {
    setUserName(null);
    setLog(false)
    
  };

  const [dataSource, setDataSource] = useState([])
  let a;
  let array = new Array;
  let array1 = new Array
  const search = () => {

    fetch("https://fakestoreapi.com/products")
      .then(response => response.json())
      .then((responseJson) => {
        console.log('getting data from fetch')
        setTimeout(() => {
          if (array.length > 0) {
            array = new Array
          }
          responseJson.map((i) => {
            array.push({ id: i.id, category: i.category, description: i.description, image: i.image, price: i.price, title: i.title })
          })

        
          array.map((i) => {
            let wordArray = (i.title).split(' ');
            a = wordArray.includes(find)
            if (a) {
              array1.push(i)
            }
          })



          console.log(array1)
          setDataSource(array1)
        }, 2000)

      })
      .catch(error => console.log(error))
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {log == false ?
        <View style={styles.container}>
          <Text style={styles.text}>Sign In</Text>

          <View style={styles.inputContainer}>
            <View style={styles.iconStyle}>
              <AntDesign name="user" size={25} color="#666" />
            </View>
            <TextInput
              value={email}
              style={styles.input}
              numberOfLines={1}
              placeholder="Username"
              placeholderTextColor="#666"
              onChangeText={(userEmail) => setEmail(userEmail)}
            />
          </View>

          <View style={styles.inputContainer}>
            <View style={styles.iconStyle}>
              <AntDesign name="lock" size={25} color="#666" />
            </View>
            <TextInput
              value={password}
              style={styles.input}
              numberOfLines={1}
              placeholder="Password"
              placeholderTextColor="#666"
              onChangeText={(userPassword) => setPassword(userPassword)}
              secureTextEntry={visible}
            />

          </View>

          <FormButton buttonTitle="SIGN IN" />


          <View style={{ margin: 45 }}>
            <LoginButton
              readPermissions={['public_profile']}
              onLoginFinished={(error, result) => {
                setLog(true)
                console.log(log)
                if (error) {
                  alert(error);
                  console.log('Login has error: ' + result.error);
                } else if (result.isCancelled) {
                  alert('Login is cancelled.');
                } else {
                  AccessToken.getCurrentAccessToken().then((data) => {
                    console.log(data.accessToken.toString());
                    const processRequest = new GraphRequest(
                      '/me?fields=name,picture.type(large)',
                      null,
                      getResponseInfo,
                    );
                    // Start the graph request.
                    new GraphRequestManager()
                      .addRequest(processRequest).start();
                  });
                }
              }}
              onLogoutFinished={onLogout}
            />
          </View>
        </View>

        :
        <View>
          <View style={{ flexDirection: 'row', padding: 10 }}>
            <Text style={{ flex: 2, marginRight: 5 ,color:'black',fontWeight:'bold'}}>{userName}</Text>
            <View style={{ flex: 1, marginRight: 15 }}>
              <LoginButton
                readPermissions={['public_profile']}
                onLogoutFinished={onLogout}
              />
            </View>


          </View>



          <View style={styles.inputContainer}>
            <TextInput
              value={find}
              style={styles.input}
              numberOfLines={1}
              placeholder="Search"
              placeholderTextColor="#666"
              onChangeText={(value) => setFind(value)}
              onEndEditing={() => search()}
            />
            <TouchableOpacity activeOpacity={0.8} style={styles.touachableButton} onPress={() => search()}>
              <AntDesign name="search1" size={20} color="#666" />
            </TouchableOpacity>


          </View>

          <ScrollView style={{ margin: 10 }}>
            <Card style={styles.cardContainer}>
              {dataSource.map((i, key) => {
                return (
                  <Card style={styles.cardView} key={key}>
                    <Card.Content style={styles.content}>
                      <View style={styles.title}>
                        <Title style={[{ color: 'grey', fontSize: 16 }]}>{i.title}</Title>
                      </View>
                      <View style={styles.image}>
                        <Image source={{ uri: i.image }} style={styles.image} />
                      </View>
                    </Card.Content>

                    <View style={[{ margin: 8 }]}>
                      <View style={styles.BenifitView}>
                        <Text style={styles.Benifittext}>Category</Text>
                        <Text style={styles.Benifitpoint}>:</Text>
                        <Text style={styles.Benifitamount}>{i.category}</Text>
                      </View>
                      <View style={styles.BenifitView}>
                        <Text style={styles.Benifittext}>Price</Text>
                        <Text style={styles.Benifitpoint}>:</Text>
                        <Text style={styles.Benifitamount}>{i.price}</Text>
                      </View>
                     

                    </View>

                  </Card>
                )
              })}

            </Card>
          </ScrollView>




        </View>
      }

    </SafeAreaView>



  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textStyle: {
    fontSize: 20,
    color: '#000',
    textAlign: 'center',
    padding: 10,
  },
  imageStyle: {
    width: 200,
    height: 300,
    resizeMode: 'contain',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 20,
  },
  footerHeading: {
    fontSize: 18,
    textAlign: 'center',
    color: 'grey',
  },
  footerText: {
    fontSize: 16,
    textAlign: 'center',
    color: 'grey',
  },


  inputContainer: {
    marginTop: 5,
    margin: 10,
    width: windowWidth / 1.2,
    height: windowHeight / 15,
    borderColor: '#ccc',
    borderRadius: 3,
    borderWidth: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center'
  },
  iconStyle: {
    padding: 10,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRightColor: '#ccc',
    borderRightWidth: 1,
    width: 50,
  },
  input: {
    padding: 10,
    flex: 1,
    fontSize: 16,
    fontFamily: 'Lato-Regular',
    color: '#333',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputField: {
    padding: 10,
    marginTop: 5,
    marginBottom: 10,
    width: windowWidth / 1.5,
    height: windowHeight / 15,
    fontSize: 16,
    borderRadius: 8,
    borderWidth: 1,
  },
  container: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },

  socialLogo: {
    width: 200,
    height: 150,
    marginRight: 10,
    marginBottom: 25,
    marginTop: -120
  },


  text: {
    fontFamily: 'Kufam-SemiBoldItalic',
    fontSize: 25,
    marginBottom: 10,
    color: '#051d5f',
    width: windowWidth / 1.2,
  },

  text1: {
    fontFamily: 'Kufam-SemiBoldItalic',
    fontSize: 15,
    marginBottom: 30,
    width: windowWidth / 1.2,
    color: '#051d5f',
  },

  navButton: {
    marginTop: 15,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#2e64e5',
    fontFamily: 'Lato-Regular',
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 35,
    justifyContent: 'center',
  },
  color_textPrivate: {
    fontSize: 13,
    fontWeight: '400',
    fontFamily: 'Lato-Regular',
    color: 'grey',
  },
  touachableButton: {
    position: 'absolute',
    right: 3,
    height: 35,
    width: 35,
    padding: 8
  },
  buttonImage: {
    resizeMode: 'contain',
    height: '100%',
    width: '100%',
  },
  cardContainer: {
    elevation: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1, },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // paddingTop:10
  },
  cardView: {
    margin: 10,
    color: 'blue',
    fontWeight: 'bold',
    resizeMode: "contain",
    elevation: 10,
    shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84,
  },
  image: {
    flex: 1,
    width: 80,
    height: 55,
    resizeMode: "contain",
    marginRight: 10
  },
  BenifitView: {
    flexDirection: 'row'
  },
  Benifittext: {
    flex: 1.5,
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
    marginLeft: 10
  },
  Benifitamount: {
    flex: 2.5,
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
    alignContent: 'flex-start'
  },
  Benifitpoint: {
    flex: 0.5,
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
    marginLeft: 10
  },
  title: {
    flex: 4
  },
});


