import 'react-native-gesture-handler';
import LoginScreen from './src/components/view/Signin';


import React, { Component, useState ,useEffect} from 'react';  
 import { Platform, StyleSheet, View, Image } from 'react-native';  
 const App = ()=>{

  const [visible,setVisible] = useState(true)

  useEffect(() => {
    setTimeout(function(){  
     Hide_Splash_Screen();  
    }, 5000);  
  }, []);

  const Hide_Splash_Screen=()=>{  
    setVisible(false)  
  }  
  

  return(
  <View style = { styles.MainContainer }>  
                <LoginScreen/> 
                 {  
                  (visible === true) ? 
                  <View style={styles.SplashScreen_RootView}>  
                  <View style={styles.SplashScreen_ChildView}>  
                        <Image source={{uri:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVsAAACRCAMAAABaFeu5AAAA6lBMVEUpf7j///////0pf7cpf7r///z8//8pgLb8//31///6//71//37//smd6Mld6n///opfrwrfaigxNYbea5rnbvj8/cdebKbvtIfd7Pl9PSrydEle7X1//kld6gkgb2gxtVcl7U/gbHI3uaQuM610drS6e0Wc6t+qsEccqSFtMj///U8hKzu9/VkmL/B2+KbxtoxgKZPjrNSlLqcxc9bn756p8S93eaoytwAaaCvzuG50NtKirSJsL7E4egadp53qMje+fZYl65Zi6NFhad4pbl+rczp/v2Itsdnnrc8irm11NgKZ5WLudTM6vPCaubYAAAV/0lEQVR4nO1cC3vaONbGkizZwiCDQNhxTLjEhgaSlrRpZtNJd2Y7vc3O/P+/850jY0JSaDNNr/vpfXoBY2T51dG5m0bDwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBweEjYOyjH1PaoI2Pn+LwAYA1BsRWf/ZDCGTX4Z+BCZq083aOaO9E3sqTWAgntv8ULDa9yZswDJt7AR8VUyO+90x/NjAm8gOulCLePhCEIumYMvoJreywBaCqnHqR7+2ndo1A/a0zwZzw3h+CLkI/Ip+k1lOhmmraiJ09uydYQ8yPicc/ySxoBl/1nziDdn8IpkeeIvcQW1QK3oEBOf/ec/5MgKVAuQB381upNZEX9+G1klwSrujPqm9ZzNBFRyf9W0mHfk2i+3LrkWCif1axpWzItNFtQ7/VzqOdlAf3FlxAz/ys5A5ZPrpKi8edb7P1aEN3/X9EbdBvCXByv8Xkviggno+znuScnBT5N+GWsRdh4N+fW4wg3pqfkFrglsatPt6DIgP99a8nGqw8Ix+Jx3aQy/3wCViDn5BdEa8CcDVJpIry6wsuE7rH7+d+1ZABJ8efmtqPSTxlC2m1ml+UX/tStMGo6ct11ADhg/Q450qSW3EEIZzgv2ulDG9IcJ59ZFyhE52YJDGglutD4PsYYxKjjfiOEk/zwveUDMhUf+VJCNGgeurVUgsCqYgPQhx6wS1ulcKjSim5PhKQ4mNze9896j49AiyG9VnA7fgp4Ojp02ffz8mglK5SDC6fa/F1dQLeI+0sNzxyNUGcpSra1hGE+IfVB4f1cV96F8necZl+vraNT83GR2e6Wynr5uI76gsq9GIwOZ61G+zrrrAYgka4PJF+TeKy1UpKU46k2k4ugEbolgnglzNVHwp4mu8fOBs3I+X7RC07Yk2uEOOlQpDX39E5pjHsH1BW2dePyyAAXMktQ8YHBp3A9rHa9neDYNmBAJxl7yWvD/uEd81+g5V0K61MBphLZ5ho00e4Sp4EI/L9Imabe2YQlMHfr7x76LicbJFIyLKDJoq+bwb+1mHvLyMEi5Pn6oZvwsMX+8dt6z7BbHqQ5nAPeBfZIggIuNHR6f+LHJqAtettp8O5Ry5LK2PdbUeBpCWcGWfnt9QwAT9sn8GnDXOBbqTy1DQHTxhWRv9LcRkocqXZ2nmgqP2qyier3laTwldsvSXqyiitnToKl9yxm+mmQsq2xgIxFfWo60FrDXVXJWD1ejOthwPLuu10e/NzFUQv7I7N0y1b5j0TNI5ZqyBBHWP44OIG/DSLd48M82sXHP0PmbZAq2Psd8IjHvDwiR0fZFdgRJ9l4KhlqP1qaWbVH+QQ5pdlqBztG6xDA1l4wGSNiiRa8YReumjgUAl+Im4SiDRmdn1wRhleB87cqmWv+wHw4pTCXIzJ7NKIT/USfAJw2cy8JVJtkQjMTUoKd5/9Wnth4CRcz3EZ9IXn3QkxinJ/QT0bgZ+MqvwvTeFS+XN04kJvmuBto+gKU7ZWs8HR5eXgYgwbo/aJ2vkc0GrE4B7m76YHV4NeywBbHWS/7FwMDg66swV8ga55ECCExrRfzAaXV5cD+Ehbqq1P0MKxXsLa0LLTm81GYH/beChv2WaA9rwFZ+QxuGaJHk0vD44ejeYJ5iDj3VJzT2pRNjqHYeRvcwtMnmaY4GwVqvoAPO2VAX9C5P07sRuR/uwjaQV9bCMOLwVHWGTnBFxjqfo2O8ngAuV4OnmzNo0kLP5drrk1j1JEvxPrztESPpTEbx7lEK0KoVfPQyx9cMKvVyYe2h0AzLYXf01CXg1GeDFrVVInxLPf7GDnSbz4E8YhfNbQU3uo6MDMzfQNvv5tTLUevKm0IO8/gnVlD6y2ilhfEnUrSQPBLwSDdJg1kp60Qgrm57KEKw0xxLjNLfeDZU73zYI2FtUI6pEeCj2B2I8EQY/aDS/iZ9eRCuB6gTpRnCjwrBva1gP0wMcI8PBJ0luqQ/DZPHIovf4qZiKZ8gCVdhBIPwpnRgyt+mGjs6ZC5vBqcIbyikVm1avu4c6R5NT8nnq2Gjiz7goYgsMcvgqv7W4S5jw98UGYOA+CyL9uPTiuENl/ZKhubXOODsAsgd2XlROb0iWkOc4Y7JDFUt2mFiYlyeMSM/m7AFR0FQexJ2nOGu88G9FdlbixYcu13njKV6iEfOSEA7lpZvuh9ADvVx6+vAhxOUgEKh4iwuWrTnlMFHh1vk3EQdz6Dv1kWNp2oQJg1AaOJyfwkVTpq8xG8z1b+fdOYfIRaCjuXRjz2PovzQ7wl3fh8p4M814T9oIPJ+DKwKZoPZBbJnQRwM3dkUY/6ueoytmrEGnn6KEymplL/4NcGcyx+WpfAp8OczCUHKmYmXmBG5k0F7E1WUyUb0gAAijteoI34QVSPW+jmTMDHDo4/E8ovZMTEOwIv+mR5/8deFEAoh6pyC6K18fgBcwQKCvwCKWUVT4ERpLquo16OOtVArM6hoMqgi/PssTKbRR2cN927V5dvgp9P5SSg4t4IpGRRw/tcDGzne0IihwZUDgUFAbOItVgdmkGIUbw4blEPS/3FUfAWM2QFN/rlxdeADfsD/RaPYuyfwJxiOxfXR4dTZoqVJFUpKc7sI8tt9EyPWnyZXGdygillKvw1ybIcFhMisMqZiTkL43LFOf9KPB92T+77MJYBEVaBaAx2Jpb0LJRgO4jqJ6LG27pmtvAXxbAsOxfFxA4hrg7/TR/iCcGhqydSn8HXSApCwEuF+28wffWXMWtSeTt4BaO9fZRS4eiLBQBb0390QeGpCryhlj7PPPfpJq8HmtjjC4XE9AuIMPXpYhZxS2HXSJnLa2T80kTzS3sWO+QHy+Mzp8MTojlpEDDiHIbBMXrRSvXgA74IxyWrdBg/CpuAwLTCPqX06Oz5iwzd7lV3Af38Pj9XJfti2WEijAK3j1I4wo92JmzhY3Oj9ugRYfmrU1zgrWCSaIV3nG+DPqoQndNhGFlPkKJAObgBonq1V4FE/M3xfk8a2C2kcbgC4OoEf8NRtaVTgBdEL7QcGWq55PIRs8gj9MSHa6sPUU17kk+xvBj2OoX70oYGqwCzATWE+gh4XsxrOUWhDac5Umi24sX4gNuPVAWcjYHJ1o0zIulbyX9Kvl8buGeFiD+u9jFia1AW8Wi1ZfBqe0VK4s99TSQyqneXevHg+VxYEfEv+r5nK3FFnTMLGe28wljiKG+QK58PspgRS23ksg/EkxMYtRx6OFG5dF1SanNA4ODCJIIewZWB1bw9UuQhSqGaFDg04pBTw/pmtuTQPXm8E3wKYRIHt/hFpgPBqUNT2Bl3uJsiVckn68UBAO9E8idfAFjsDtBLSQzD9SpYMNktq8uobyo3xH7ki+MLoK6Nk8OX9SlVZDVYWbXtzpA6Tgk6EmjWwXc2msVdT2Lgf+GZlTJVYbBGEiuGZATVFFwPoYJ2XB44wgyHVr3BnPfa24jcgVu5DqcMHe55Sdh2l6HeJR1Irt30+Tz82Qwvz4Pmnu4DXgPxYPN/34BdwJeTirVzhPB+1XhSuytR1PdrfMSsqvZTcoAlyOu406hS7sVvSkYoJrbt3Umkplu5SfDglfJAhr3/AhMK8ckG/i9oKWr6h0GyeaX1I51ZMRabr1Ivrc5jYrb7h1uifKmYGOHNn0f6wnKFj/8fJUA8ymXkhzukcbg5NqgbIuFxnRc8prsphYzEL4c6b2uwpDmy5rbzkYSMAYeYjk7MxoMyPjZoy4mgbg3SOBilb4NRnVdg2UXVcA1rbwM0MnxgmPkIbs28w6BDexoNGTz1qL3qIvrRLCxquY2mMB51Rx3cAtqbUXXiwaHDvALXnP82YEZeAllIb3dOgHuJHiWYQE4/3tFURHpVO5dBU+uPkgq3ZBrphtX5EjX6V5q24aSZPzs7dWkD1GEItVMBqbm1msuaslhGUYAMKsLPayVSi4DsFiYQRZ4LxTG6k2PYSwORtOGPFvcwnlJnTyj9EO55c0OrZNsDbw81rEXn60SwOM2R74K9onjRGPSzqC+hWvE4Kju8NasfJ3ItE336VtBF2m9KBA4bFLpgsaZmRaVUwfel1wvMmbma247os4yIkMo1qObtt88DLBbBRxxirmJfFaEdigIEPx1QukATOGaW+8iq68Mcju4y20Q5nSz6snUJkqan88t+pmLNyf7WpnlC9oYDkWeSm7dV/AT9iwCicCgNPYWEtpnNg+MUseDs7LWoEKYXupXGYYAVHYd8d1wi7K0HoOtd/Y2t+3Qlv2BW7i2GfWlTzATAIGeDLiv1tzSOi473fR57NK3JBxvMk5sze0D5Baxz78FRXbcHg7Bi38MRq1oiRj04zvu7WzPhdBqX52fxlS/C8BIw/ZoQqguwS2qctWMtmZYpfdBZWJDD5FLxe9we9jZ3G72rvKLRtlws2ZNDmrSym3D/BFyn2ASx+adeBrYsQ4M28S8p7R+cGs3t+31HsE8yhRcOO7J8UOoBRue7nJauSLhK1uy61hTNzMxuAGtMwj5d5xN+NYd3waM0EqVJ1UzfUewJwDissoTpvoc3D+fEAlR7dVgdvp+XqmOLZ1w2KnHYbS3m9tKbsXqUGKShRMYqwtjtap21xtuA++00bgnt8LK7YO5bbT/vTNFIL2BjjFZegm7zidLLUCGs1Vz18keP9N7atEY8l56nCvFZ78U8L+nyNRYg0zh9iGWCKKzUT4H8w4hcFXmuCW3G26zdd1pdFPlaDeJWutb/RzXSPmTU91GX6FRrrnVNbcecEvpXm49tdG3IA4bbh9WdxDl9a5dHqYak7JiFXDOA9/r4tvMHOzUCaHNlO4cvhGPSATm3C/m2SmW4nzSfGJrJZj4AZMjj+boGGMYaz6PW/AT6HvMywbqoLQ1H0xV2Ga6LW6Jd5p9Y27BUf5d8ii44wFIZVNIDSDeOjO8uciw4PVk6d0BKDnSbe8LyRhN+j5QyknPMH1tFSqvusiSKbiniqct6xhhgV0v7+gE4HZNpGjs5BYTtsiteQRE+KHs1EVHNu+rD7jdNHrck1v+AP/WDkRZfhx5d/vFCdYdYKu9I02bC5D+vzDYFnr6gbr1VdoRw11yi8U8M1BYUfAnEG82znmA3g5EBMjtsQcrGkxKjOBtffWF/Ci3ZC+34EoSzGhvmh4EG4fBh9w2bri964N9BW5BNAV9wuXtjDe8GYGLLuK84CeW9iji5zmLGbtd+q1CjFkmdvV/YHoKNisE9uAKjFgsmD6rvoTdgzQ5A0NE+Fkds8P98k9zy0c3tTnkliC3tLyKsNBQlHYsDEkeeXe49W7kFgLf+8rtQ2rpTFR13kht08UhbEBhSn71oqopF+z7ZM7iONZ/3K7zAjsFsLNzChCFlhPQtXDaGWYBKF3ZdgXFH2HoNQG5VV7RssXsmInxuiXtozoBs2S7uAWbAPpljg/gYEq/nZIdtmyTyvgm3FrQvL8dmoH2xbABTFfnlpBiABE3Xl7724EcaNtzs7tHBlw4W9TAwtcqs90crWNsBAnUmye0oY9xNyj5rhQdiOGzTlG5pMjtuqZzw20d88KGupHbvI4dQCcoguWyWSnokAqaT6JNzLuJy04Z3XSlfZAH28WtLx/MLdxmz9u2ZYpfJpj4Qi22xWKqBW6n0a1mBuJfGbF7BthNsLSOPPbeYL4WBPfQB6uj/ANDs6kvkY/mTCfGlL1CharWCffktrnmdmguPIWRQzhrGxjt94KENrt9i9veJ7gdfwVu4VLz69v9YGMBGjRbhNvJGazNYeuJPo7qXjuszULMvS9JA1vVnugvX60vRM0BeM5cRuFKZy/CICAS3qZ/Hhz3uc+jk225JRW3a68JubVOBnBbX66OecthoxOCepBgNNOzgz/7EINj9tFyK250QlYLgWhYbj3klmLXKhb0wnatMarYweN++EAfDAAOwYpzr5ZGjt49GKfy9gOoMlrmaNDFItpkuiGqGpjdvQnY/9MjtjiLWdSGLZoLuoikbXmamCw5Pgmlzwm6phyERB2ktgVnAFfXA8/3fa/5pLbsVt/64BCPbp7+yEPUN6BvhyK5AlcHC8BYswad5l/2sQ/bcls9beBziB1qbkXyGI4o5NZ2BOPIYb7O7gv0D+EQbKnFgx+1Z8NG++gmHSaX89zoshw1b7Ut+E1y8EtZlvqXs01Wkkf91s4haTwErdePbGfAcpNxAWegi13nAVEXsAn68Fphhy6RWD2HmBfbC8BfHWYDXNbo8Mmm4zzuVeX7c13fLzMwQRlwzN/STl9FvjqBBVGRJGTy3xSWDIIJI6ju+T62LpxmNzEv1nTAaWtmGHpi5t5Xy81zA4zpKbjkSoEP9mCdAI5oZ7khkpwUZ2dn2Dce3NK3QdCcXMMnZzdOReBdZLvDBtDX+ohEEUrPYFPSg/3caeKhSKYdJt73bfIYuzB8cqXzJQGe+eOsIxLb+8EPn2yyvabHA5TEEY3X5LJcgnZSNi6j2aLwZIDpCQKMP9dlCtz4ILcg8c/WvR9ZnasBue3isSjMMT7C1zIIa4sM3BrUCVI+NJ9QTZOZR5scOW9Gnk0KQrTrbXGrAmvEwEnfnOlP8j2VHLDWq9/6aR9QdG6KYrHQb/FgmqZTTUVnsIzsYLLolSwr8PTl1AyH+i982YeoZOPuP3uDX/wNHO/6Gh17Tn+qbRdo/rSPfhP2Tc9ykf9tv/+03Ri2e3Ya6elmpkKUf9ljf6MtS6Z2Rn/XHwtGzevfcI798cNbRdHhahf1M1Ck1g78du0RxcsyTqruMThLnu9rGrfPp5T4+zaYhWGbgiOYFlA3OilhI4OeT8zp9PKy+2gFZ1EBn8BnOobbM7oNL/PhzYMSmcYv6i3DKXI4guNjEgZsf2s07eJYZSbiaiyDqV1KSwu22eAwITuY0Va74usyuSnkwaZr63IOJ3yhHzzK111Tt7G7vG4BYuwd63G8v163bk1mW/xvr0TVx5UZuMcs2zQ9rbn78KbWH2xbF7b9ggls8Wxrs/X1+/0a1D579aV+MwbipDN+t7P2owg8dTh+8G/WCDEcsiE+R/HgGwEVH8fDL0XIlwNtxNmrprevILYTKnrbGu5M0tz/spj1E+LLPOQlxI/HKwIZMt3A+0fc2lbHL3DtL/Qg0jf7MYTPQWfZ/AfPoUvS+1K6/kvhhxRbC2Zm/n1+CKgW28n8m/0iyU8PwXJsc76X6OLTvqu9nTQOHyA2I3J4v5+skRCql1/5Wdj/JVA2Tp5zvq8x6Ra1Klp2hl/9gc3/HWCP96Kp+K4q+R34Uk1N4+f9fbBvDuxN1W83vz3xEWWLnTZtQdkPbJh/MGCXH9WXAcptEGDSyUes/7Xv4B9siuOqGNMf2eX5MSHK0zP8/dv9aDYPi6nJnT74p6DDhjatly9ftvai02qVutHY87Cew36I9QP1+0Jzm1Wh2ALv5Nbh58MXyAk6ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4fGn8H4s0pYKf01wHAAAAAElFTkSuQmCC'}}  
                     style={{width:'100%', height: '100%', resizeMode: 'contain'}} />  
                 </View>  
              </View> 
               : null  
                }  
            </View> 
  )
 } 

 export default App;
 
  
 const styles = StyleSheet.create(  
{  
    MainContainer:  
    {  
        flex: 1,  
        justifyContent: 'center',  
        alignItems: 'center',  
        paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0  
    },  
   
    SplashScreen_RootView:  
    {  
        justifyContent: 'center',  
        flex:1,  
        margin: 10,  
        position: 'absolute',  
        width: '100%',  
        height: '100%',  
      },  
   
    SplashScreen_ChildView:  
    {  
        justifyContent: 'center',  
        alignItems: 'center',  
        backgroundColor: '#00BCD4',  
        flex:1,  
    },  
});  
